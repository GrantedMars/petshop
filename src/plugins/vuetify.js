import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/dist/vuetify.min.css'


Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: { 
    dark: true,
    themes: {
      dark: {
        primary: '#FF9100',
        secondary: '#b0bec5',
        anchor: '#FF9100',
      }
    },
  }

});
