import Pacotes from '@/components/Pacotes';

const pacotesRoutes = [
    {
      path: '/pacotes',
      name: 'pacotes',
      title: 'Pacotes',
      icon: "mdi-folder-open",
      component: Pacotes,
    }
];

export { 
  pacotesRoutes
}