import Usuarios from '@/components/Usuarios';

const usuariosRoutes = [
    {
      path: '/usuarios',
      name: 'usuarios',
      title: 'Usuarios',
      icon: "mdi-account-card-details",
      component: Usuarios,
    }
];

export { 
  usuariosRoutes
}