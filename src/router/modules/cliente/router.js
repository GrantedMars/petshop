import Cliente from '@/components/Cliente';

const clienteRoutes = [
    {
      path: '/cliente',
      name: 'cliente',
      title: 'Cliente',
      icon: "mdi-account-plus",
      component: Cliente,
    }
];

export { 
    clienteRoutes
}