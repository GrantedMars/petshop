import Racas from '@/components/Racas';

const racasRoutes = [
    {
      path: '/racas',
      name: 'racas',
      title: 'Racas',
      icon: "mdi-dog-side",
      component: Racas,
    }
];

export { 
  racasRoutes
}