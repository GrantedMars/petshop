import Servicos from '@/components/Servicos';

const servicosRoutes = [
    {
      path: '/servicos',
      name: 'servicos',
      title: 'Servicos',
      icon: "mdi-briefcase-check",
      component: Servicos,
    }
];

export { 
  servicosRoutes
}