import Pets from '@/components/Pets';

const petsRoutes = [
    {
      path: '/pets',
      name: 'pets',
      title: 'Pets',
      icon: "mdi-paw",
      component: Pets,
    }
];

export { 
  petsRoutes
}