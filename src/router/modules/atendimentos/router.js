import Atendimento from '@/components/Atendimento';

const atendimentoRoutes = [
    {
      path: '/atendimento',
      name: 'atendimento',
      title: 'Atendimento',
      icon: "mdi-clipboard-text",
      component: Atendimento,
    }
];

export { 
    atendimentoRoutes
}