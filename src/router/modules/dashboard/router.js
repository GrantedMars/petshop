import Dashboard from '@/components/DashBoard';

const dashboardRoutes = [
    {
      path: '/',
      name: 'dashboard',
      title: 'Dashboard',
      icon: "mdi-view-dashboard",
      component: Dashboard,
    }
];

export { 
  dashboardRoutes
}