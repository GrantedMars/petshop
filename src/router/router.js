
import { clienteRoutes }  from '../router/modules/cliente/router'
import { dashboardRoutes } from '../router/modules/dashboard/router'
import { atendimentoRoutes } from '../router/modules/atendimentos/router'
import { pacotesRoutes } from '../router/modules/pacotes/router'
import { petsRoutes } from '../router/modules/pets/router'
import { racasRoutes } from '../router/modules/racas/router'
import { servicosRoutes } from '../router/modules/servicos/router'
import { usuariosRoutes } from '../router/modules/usuarios/router'

const routes = [].concat(
  dashboardRoutes, 
  atendimentoRoutes,
  clienteRoutes, 
  pacotesRoutes,
  petsRoutes,
  racasRoutes,
  servicosRoutes,
  usuariosRoutes);

export {
  routes
}